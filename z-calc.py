#! /usr/bin/env python
from os import system as s

def zprof(penghasilan, pengeluaran, nisab):
    pb = penghasilan - pengeluaran
    ns = nisab
    if ns <= pb:
        zakat = 0.025 * pb
        zakat = int(zakat)
        zakat = "{:,}".format(zakat)
        ns = "{:,}".format(ns)
        pb = "{:,}".format(pb)
        print(f"Penghasilan bersih anda bulan ini adalah sebesar Rp.{pb}")
        print (f"Zakat Pendapatan pada bulan ini yang harus di keluarkan adalah sebesar Rp.{zakat} sesuai dengan nishab sebesar Rp.{ns}")
    else:
        ns = "{:,}".format(ns)
        pb = "{:,}".format(pb)
        print(f"Penghasilan bersih anda bulan ini adalah sebesar Rp.{pb}")
        print(f"Anda tidak diwajibkan mengeluarkan Zakat Pendapatan karena tidak mencapai nishab sebesar Rp.{ns}")

def zmaal(kekayaan, nisab):
    tk = kekayaan
    ns = nisab
    if nisab <= tk:
        zakat = 0.025 * tk
        zakat = int(zakat)
        zakat = "{:,}".format(zakat)
        ns = "{:,}".format(ns)
        tk = "{:,}".format(tk)
        print(f"Total kekayaan anda dalam satu tahun ini adalah sebesar {tk}")
        print(f"Zakat Maal pada tahun ini yang harus di keluarkan adalah sebesar Rp.{zakat} sesuai dengan nishab sebesar Rp.{ns}")
    else:
        ns = "{:,}".format(ns)
        tk = "{:,}".format(tk)
        print(f"Total kekayaan anda dalam satu tahun ini adalah sebesar {tk}")
        print(f"Anda tidak diwajibkan mengeluarkan Zakat Pendapatan karena tidak mencapai nishab sebesar Rp.{ns}")

def zfitrah(hrg_beras):
    zakat = 3.5 * hrg_beras
    zakat = "{:,}".format(zakat)
    print("Zakat Fitrah yang harus di keluarkan adalah sebesar Rp.{zakat}")

def hitung_zakat():
    while True:
        s("clear")
        print("""
███████╗      ██████╗ █████╗ ██╗      ██████╗
╚══███╔╝     ██╔════╝██╔══██╗██║     ██╔════╝
  ███╔╝█████╗██║     ███████║██║     ██║
 ███╔╝ ╚════╝██║     ██╔══██║██║     ██║
███████╗     ╚██████╗██║  ██║███████╗╚██████╗
╚══════╝      ╚═════╝╚═╝  ╚═╝╚══════╝ ╚═════╝
Kalkultor Zakat dengan perhitungan nishab :
*) Zakat Pendapatan = 653 kg beras
*) Zakat Maal = 85 gram emas
perhitungan Zakat Fitrah = 3,5 kg beras
========== Menu ==========
[1] Zakat Pendapatan
[2] Zakat Maal / Kekayaan
[3] Zakat Fitrah
[ctrl+c] Keluar
==========================
""")
        zakat = input("Hitung Zakat (1-3) ? > ")
        if zakat == "1":
            nisab = input("Harga beras /kg yang anda konsumsi ?  Rp. ").replace(".", "")
            nisab = int(nisab)
            nisab = 653 * nisab
            penghasilan = input("Penghasilan Anda Bulan ini ? Rp. ").replace(".", "")
            penghasilan = int(penghasilan)
            pengeluaran = input("Pengeluaran Anda Bulan ini ? Rp. ").replace(".", "")
            pengeluaran = int(pengeluaran)
            zprof(penghasilan, pengeluaran, nisab)
            opt = input("Hitung zakat yang lain (y/n) ? > ")
            if opt == "y":
                hitung_zakat()
            else:
                exit()
        elif zakat == "2":
            nisab = input("Harga emas /gram saat ini ? Rp. ").replace(".", "")
            nisab = int(nisab)
            nisab = 85 * nisab
            kekayaan = input("Total kekayaan anda dalam tahun ini ? Rp. ").replace(".", "")
            kekayaan = int(kekayaan)
            zmaal(kekayaan, nisab)
            opt = input("Hitung zakat yang lain (y/n) ? > ")
            if opt == "y":
                hitung_zakat()
            else:
                exit()
        elif zakat == "3":
            hrg_beras = input("Harga beras /kg yang anda konsumsi ? Rp. ").replace(".", "")
            hrg_beras = int(hrg_beras)
            zfitrah(hrg_beras)
            opt = input("Hitung zakat yang lain (y/n) ? > ")
            if opt == "y":
                hitung_zakat()
            else:
                exit()
        else:
            exit()
        
try:
    hitung_zakat()
except KeyboardInterrupt:
    print("^")
